#ifndef LMATH_PHASE_H
#define LMATH_PHASE_H

#ifdef __cplusplus
extern "C" {
#endif

/** Способ рассчета фазы комплексного числа */
typedef enum {    
    LMATH_PHASE_MODE_COS = 0,
    LMATH_PHASE_MODE_SIN = 1
} t_lmath_phase_mode;


double lmath_phase_calc(double r, double im, t_lmath_phase_mode mode);

double lmath_phase_normalize(double phi);
double lmath_phase_delay(double f1, double phi1, double f2, double phi2);

double lmath_phase_conv_degree(double rad);
double lmath_phase_conv_radian(double degree);

#ifdef __cplusplus
}
#endif


#endif // LMATH_PHASE_H
