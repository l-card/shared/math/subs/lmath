#ifndef LMATH_INTERNAL_H
#define LMATH_INTERNAL_H

#include <math.h>

#ifndef M_PI
    #define M_PI 3.1415926535897932384626433832795
#endif

#ifndef MAX
    #define MAX(x,y) ((x) > (y) ? (x) : (y))
#endif


#endif // LMATH_INTERNAL_H
