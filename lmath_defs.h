#ifndef LMATH_DEFS_H
#define LMATH_DEFS_H

/** Коды ошибок */
typedef enum {
    LMATH_ERR_OK              = 0,
    LMATH_ERR_INVALID_WINTYPE = -20000, /**< Неверно задан тип окна */
    LMATH_ERR_MEMORY_ALLOC    = -20001, /**< Ошибка выделения памяти */
    LMATH_ERR_INVALID_PARAMS  = -20002, /**< Неверно заданы входные параметры */
    LMATH_ERR_INTERNAL_CALC   = -20003, /**< Ошибка при вычислениях */
    LMATH_ERR_PRECISION       = -20004  /**< Не удалось добиться указанной точности */
} t_lmath_errs;


#endif // LMATH_DEFS_H
