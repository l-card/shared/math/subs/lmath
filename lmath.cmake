# Файл для включения в проект на CMAKE, чтобы встроить библиотеку в код программы.
# Перед включением необходимо определить переменные
#    LMATH_USE_FFTW - включать ли код, использующий библиотеку FFTW
#    LMATH_USE_MKL  - включать ли код, использующий библиотеку MKL
#
# После включения будут установлены следующие перменные:
#   LMATH_SOURCES       - используемые файлы для сборки (заголовки, исходные коды)
#   LMATH_INCLUDE_DIRS  - директории с заголовками
#   LMATH_LIBRARIES     - используемые библиотеки
#   LMATH_LIBRARY_DIRS  - директории с библиотеками
#   LMATH_CONFIG_FILE   - результирующий файл конфигурации

set(LMATH_DIR ${CMAKE_CURRENT_LIST_DIR})

set(LMATH_INCLUDE_DIRS ${LMATH_DIR})

set(LMATH_SOURCES
    ${LMATH_DIR}/lmath.h
    ${LMATH_DIR}/lmath_defs.h
    ${LMATH_DIR}/lmath_internal.h
    ${LMATH_DIR}/lmath_phase.h
    ${LMATH_DIR}/lmath_phase.c
    ${LMATH_DIR}/lmath_ratio.h
    ${LMATH_DIR}/lmath_ratio.c
    ${LMATH_DIR}/lmath_window.h
    ${LMATH_DIR}/lmath_window.c
    ${LMATH_DIR}/lmath_acdc.h
    ${LMATH_DIR}/lmath_acdc.c
    ${LMATH_DIR}/lmath_fir.h
    ${LMATH_DIR}/lmath_fir.c
    ${LMATH_DIR}/lmath_spectrum_freq.h
    ${LMATH_DIR}/lmath_spectrum_freq.c
    ${LMATH_DIR}/lmath_spectrum_signoise.h
    ${LMATH_DIR}/lmath_spectrum_signoise.c
    ${LMATH_DIR}/lmath_goertzel.c
    ${LMATH_DIR}/lmath_goertzel.h

    )


get_filename_component(LMATH_MODULES_PATH "${LMATH_DIR}/cmake/Modules/" ABSOLUTE)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${LMATH_MODULES_PATH})


if(LMATH_USE_FFTW)
    find_package(FFTW3 REQUIRED)

    set(LMATH_INCLUDE_DIRS ${LMATH_INCLUDE_DIRS} ${FFTW3_INCLUDE_DIRS})
    set(LMATH_LIBRARIES ${LMATH_LIBRARIES} ${FFTW3_LIBRARIES})
    set(LMATH_SOURCES ${LMATH_SOURCES}
        ${LMATH_DIR}/lmath_fft_fftw.c)
    set(LMATH_USE_FFT ON)
endif(LMATH_USE_FFTW)

if(LMATH_USE_MKL)
    find_package(MKL REQUIRED)

    set(LMATH_INCLUDE_DIRS ${LMATH_INCLUDE_DIRS} ${MKL_INCLUDE_DIRS})
    set(LMATH_LIBRARIES ${LMATH_LIBRARIES} ${MKL_LIBRARIES})
    set(LMATH_LIBRARY_DIRS ${LMATH_LIBRARY_DIRS} ${MKL_LIBRARY_DIRS})
    #если включен fftw, то используем его для fft, иначе - mkl
    if (NOT LMATH_USE_FFTW)
        set(LMATH_USE_FFT ON)
        set(LMATH_SOURCES ${LMATH_SOURCES}
            ${LMATH_DIR}/lmath_fft_mkl.c)
    endif(NOT LMATH_USE_FFTW)
endif(LMATH_USE_MKL)

if(LMATH_USE_FFT)
    set(LMATH_SOURCES ${LMATH_SOURCES}
        ${LMATH_DIR}/lmath_fft.h
        ${LMATH_DIR}/lmath_fft.c
        )
endif(LMATH_USE_FFT)


if(UNIX)
    set(LMATH_LIBRARIES ${LMATH_LIBRARIES} m)
endif(UNIX)



configure_file(${LMATH_DIR}/lmath_config.h.in ${CMAKE_CURRENT_BINARY_DIR}/lmath_config.h)
set(LMATH_CONFIG_FILE ${CMAKE_CURRENT_BINARY_DIR}/lmath_config.h)
set(LMATH_GENFILES    ${LMATH_GENFILES} ${LMATH_CONFIG_FILE})
set(LMATH_INCLUDE_DIRS ${LMATH_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR})

set(LMATH_FILES  ${LMATH_SOURCES} ${LMATH_GENFILES})
