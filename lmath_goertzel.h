#ifndef LMATH_GOERTZEL_H
#define LMATH_GOERTZEL_H

#include "lmath_defs.h"
#include "lmath_phase.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct st_lmath_goertzel_ctx *t_lmath_goertzel_ctx;

t_lmath_goertzel_ctx lmath_goertzel_ctx_create(int size, double dt, double f);
void lmath_goertzel_ctx_destroy(t_lmath_goertzel_ctx winctx);

void lmath_goertzel_process(t_lmath_goertzel_ctx ctx, const double *sig, int sig_size);
t_lmath_errs lmath_goertzel_result(t_lmath_goertzel_ctx ctx, double *r, double *im);
t_lmath_errs lmath_goertzel_result_amp_pha(t_lmath_goertzel_ctx ctx,
                                           t_lmath_phase_mode pha_mode,
                                           double *amp, double *pha);


#ifdef __cplusplus
}
#endif

#endif // LMATH_GOERTZEL_H
