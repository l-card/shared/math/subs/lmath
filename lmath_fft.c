#include "lmath_fft.h"
#include "lmath_window.h"
#include <stdlib.h>
#include <string.h>


t_lmath_errs lmath_amp_pha_spectrum_peak(const double *src, int size, double dt, int flags,
                                         t_lmath_phase_mode pha_mode,
                                         double *amp, double *pha, double *df) {
    t_lmath_errs err = LMATH_ERR_OK;
    t_lmath_fft_r2c_ctx ctx = lmath_fft_r2c_ctx_create(size, flags);
    if (ctx == NULL) {
        err = LMATH_ERR_MEMORY_ALLOC;
    } else {
        memcpy(lmath_fft_r2c_ctx_in_buf(ctx), src, sizeof(double)*size);
        err = lmath_fft_r2c_calc_amp_pha_spectrum_peak(ctx, pha_mode, amp, pha);
        if (df != NULL)
            *df = lmath_fft_r2c_ctx_df(ctx, dt);
        lmath_fft_r2c_ctx_destroy(ctx);
    }

    return err;
}

t_lmath_errs lmath_amp_pha_spectrum_rms(const double *src, int size, double dt, int flags,
                                        t_lmath_phase_mode pha_mode,
                                        double *amp, double *pha, double *df) {
    t_lmath_errs err = LMATH_ERR_OK;
    t_lmath_fft_r2c_ctx ctx = lmath_fft_r2c_ctx_create(size, flags);
    if (ctx == NULL) {
        err = LMATH_ERR_MEMORY_ALLOC;
    } else {
        memcpy(lmath_fft_r2c_ctx_in_buf(ctx), src, sizeof(double)*size);
        err = lmath_fft_r2c_calc_amp_pha_spectrum_rms(ctx, pha_mode, amp, pha);
        if (df != NULL)
            *df = lmath_fft_r2c_ctx_df(ctx, dt);
        lmath_fft_r2c_ctx_destroy(ctx);
    }

    return err;
}

