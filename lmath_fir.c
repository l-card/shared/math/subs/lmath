#include "lmath_fir.h"


double lmath_proc_fir(const double *coef, double *prev_x, int ntap, double new_x) {
    double y=0;            //output sample
    int n;

    //shift the old samples
    for(n=ntap-1; n>0; n--)
       prev_x[n] = prev_x[n-1];

    //Calculate the new output
    prev_x[0] = new_x;
    for(n=0; n<ntap; n++)
        y += coef[n] * prev_x[n];

    return y;
}
