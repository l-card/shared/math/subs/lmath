#ifndef LTEST_MATH_H
#define LTEST_MATH_H

#include "lmath_config.h"

#include "lmath_defs.h"
#include "lmath_phase.h"
#include "lmath_ratio.h"
#include "lmath_window.h"
#include "lmath_acdc.h"
#include "lmath_fir.h"
#include "lmath_goertzel.h"
#include "lmath_fft.h"
#include "lmath_spectrum_freq.h"
#include "lmath_spectrum_signoise.h"

#endif // LTEST_MATH_H
