#ifndef LMATH_RATIO_H
#define LMATH_RATIO_H

#include "lmath_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Вычисление наибольшего общего делителя (НОД)  */
int lmath_gcd(int a, int b);
/** @brief Вычисление наименьшего общего кратного (НОК)  */
int lmath_lcm(int a, int b);
/** @brief Вычисление ближайшей рациональной дроби, соответствующей вещественному числу

    Поиск ближайшей к переданному вещественному числу дроби n/d (теория:
    https://en.wikipedia.org/wiki/Continued_fraction#Best_rational_approximations)

    @param[in] val      Исходное вещественное число, для которого нужно найти ближайшую дробь
    @param[out] n       Полученный числитель дроби
    @param[out] d       Полученный знаменатель дроби
    @param[in]  max_d   Максимально допустимый знаменатель
    @param[in]  rel_err Допустимая относительная ошибка полученной дроби
    @return             Код ошибки
    ***************************************************************************/
t_lmath_errs lmath_rational_approx(double val, long *n, long *d, long max_d, double rel_err);

#ifdef __cplusplus
}
#endif

#endif // LMATH_RATIO_H
