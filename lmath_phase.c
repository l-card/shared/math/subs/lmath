#include "lmath_phase.h"
#include "lmath_internal.h"
#include <math.h>

double lmath_phase_calc(double r, double im, t_lmath_phase_mode mode) {
    double res = 0;
    if (mode == LMATH_PHASE_MODE_SIN) {
        res = atan2(r, -im);
    } else {
        res = atan2(im, r);
    }
    return res;
}

double lmath_phase_normalize(double phi) {
    while (phi > M_PI)
        phi -= 2*M_PI;
    while (phi <= -M_PI)
        phi += 2*M_PI;
    return phi;
}

double lmath_phase_conv_degree(double rad) {
    return 180. * rad / M_PI;
}

double lmath_phase_conv_radian(double degree) {
    return M_PI * degree / 180.;
}


double lmath_phase_delay(double f1, double phi1, double f2, double phi2) {
    return lmath_phase_normalize(f2/f1*phi1 - phi2);
}
